#! /bin/sh

echo "Have you kinited today?"
export DMTWEBSERVER=llodmt.ligo-la.caltech.edu:443/dmtview/LLO,marble.ligo-wa.caltech.edu:443/dmtview/LHO

#This fails. Wait for Keith.
ecp-cookie-init -i login.ligo.org -k https://llodmt.ligo-la.caltech.edu:443/dmtview/LLO

export DMTWEBCLIENT_COOKIE=/tmp/ecpcookie.u`id -u controls`

#run dmt inside of a detached screen
echo "running dmtviewer in a detached screen. To list screen sessions type  -ls"
screen -d -m -S BNSRANG dmtviewer -v -run S7_FOM_RANGE_DMT.xml

#sleep 3

# this command select the wondow of interest and move it to window1 of this computer
# the next line blows up the window to fill the screen (1920x1080)
# wmctrl -F -r OPS_OVERVIEW_CUSTOM.adl -t 1
# wmctrl -F -r OPS_OVERVIEW_CUSTOM.adl -e 0,0,0,1920,1080

# This command moves opsoveview to the top left monitor
#wmctrl -F -r "CDS Diagnostic Test Tools" -e 0,1875,0,1920,1080

