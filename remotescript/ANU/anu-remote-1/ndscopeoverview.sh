#! /bin/sh

# this command pulls up an OPS_OVERVIEW screen inside of a detached screen
echo "running ndscope in a detached screen. To list screen sessions type $screen -ls"
echo "If nothing happens, check if you have kinit-ed into LIGO.ORG"
screen -d -m -S ndscopebuildups ndscope --nds nds.ligo-wa.caltech.edu:31200 main_screen_ndscope_1p5min.yml 

sleep 3

# this command select the window of interest and move it to window2 of this computer
# the next line blows up the window to fill the screen (1920x1080)
# wmctrl -F -r "ndscope: main_screen_ndscope_1p5min.yml" -t 2
# wmctrl -F -r "ndscope: main_screen_ndscope_1p5min.yml" -e 0,0,0,1920,1080

# This command moves opsoveview to the top right monitor
wmctrl -F -r "ndscope: main_screen_ndscope_1p5min.yml" -e 0,3840,0,1920,1080
