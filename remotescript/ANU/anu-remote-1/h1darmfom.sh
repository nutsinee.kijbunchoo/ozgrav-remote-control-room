#! /bin/sh

# this command pulls up an OPS_OVERVIEW screen inside of a detached screen
screen -d -m -S H1DARM diaggui -v -a H1_DARM_FOM_nds2.xml

echo "running dtt in a detached screen. To list screen sessions type  -ls"
echo "waiting for dtt. Sleep for 3s"
echo "If nothing happens, check if you have kinited"

sleep 3

# this command select the wondow of interest and move it to window1 of this computer
# the next line blows up the window to fill the screen (1920x1080)
# wmctrl -F -r OPS_OVERVIEW_CUSTOM.adl -t 1
# wmctrl -F -r OPS_OVERVIEW_CUSTOM.adl -e 0,0,0,1920,1080

# This command moves opsoveview to the top left monitor
wmctrl -F -r "CDS Diagnostic Test Tools" -e 0,1920,0,1920,1080

