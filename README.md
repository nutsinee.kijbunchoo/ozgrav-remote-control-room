# This is a git repository for remote control room scripts -- for commissioning and outreach purposes.

This git visibility is set to public. Anyone can clone.
To make changes, you need an account on git.ligo.org

## Scripts for ANU remote control room can be found in ANU folder inside of /remotescript
